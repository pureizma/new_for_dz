package config;

import org.aeonbits.owner.Config;
import org.aeonbits.owner.Config.Sources;
@Sources("classpath:config/loca2l.properties")
public interface ProjectConfig extends Config {

    @Key("base_url")
    String baseUrl();
}
