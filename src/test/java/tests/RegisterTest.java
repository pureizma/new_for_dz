package tests;
import org.aeonbits.owner.ConfigFactory;
import config.ProjectConfig;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.Test;
import config.TestConfig;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

public class RegisterTest {

    @Test
     void testRegisterUser_Positive() {
        RestAssured.baseURI = TestConfig.BASE_URI;

        given()
                .contentType(ContentType.JSON)
                .body("{ \"email\": \"eve.holt@reqres.in\", \"password\": \"pistol\" }")
                .when()
                .post("/api/register")
                .then()
                .statusCode(200)
                .body("id", notNullValue())
                .body("token", notNullValue());
    }

    @Test
     void testRegisterUser_Negative() {
        RestAssured.baseURI = TestConfig.BASE_URI;

        given()
                .contentType(ContentType.JSON)
                .body("{ \"email\": \"sydney@fife\" }")
                .when()
                .post("/api/register")
                .then()
                .statusCode(400)
                .body("error", equalTo("Missing password"));
    }
    @Test
     void test_users_positive(){
        RestAssured.baseURI = TestConfig.BASE_URI;
        given()
                .contentType(ContentType.JSON)
                .body("{\"name\": \"morpheus\", \"job\": \"zion resident\" }")
                .when()
                .put("api/users/2")
                .then()
                .statusCode(200)
                .body("name", equalTo("morpheus"))
                .body("job", equalTo("zion resident"))
                .body("updatedAt", notNullValue());
    }


    @Test
     void testDeleteExistingUser() {
        RestAssured.baseURI = TestConfig.BASE_URI;
        int userId = 2; // Замените на существующий id пользователя

        given()
                .when()
                .delete("/api/users/" + userId)
                .then()
                .statusCode(204); // Ожидаемый код состояния 204 - успешное удаление
    }
    @Test
     void testUpdateUser_Negative() {
        ProjectConfig config = ConfigFactory.create(ProjectConfig.class);
        RestAssured.baseURI = config.baseUrl();
        int userId = 23; // Несуществующий id пользователя

        given()
                .contentType(ContentType.JSON)
                .body("{ \"name\": \"21\", \"job\": \"000\" }")
                .when()
                .put("/api/users/" + userId)
                .then()
                .statusCode(404);

    }
    @Test
     void testDeleteNonExistingUser() {
        ProjectConfig config = ConfigFactory.create(ProjectConfig.class);
        RestAssured.baseURI = config.baseUrl();
        int userId = 23; // несуществующий id пользователя

        given()
                .when()
                .delete("/api/users/" + userId)
                .then()
                .statusCode(404);

    }
    @Test
     void testGetExistingUser() {
        // Создаем экземпляр интерфейса ProjectConfig
        ProjectConfig config = ConfigFactory.create(ProjectConfig.class);

        // Получаем базовый URL из конфигурации
        String baseUrl = config.baseUrl();
        int userId = 2; // Замените на существующий id пользователя
        // Используем базовый URL для отправки HTTP-запроса
        given()
                .log().all() // включаем логирование
                .baseUri(baseUrl) // Устанавливаем базовый URL
                .when()
                .get("/api/users/" + userId)
                .then()
                .statusCode(200)
                .body("data.id", equalTo(userId))
                .body("data.email", notNullValue())
                .body("data.first_name", notNullValue())
                .body("data.last_name", notNullValue())
                .body("data.avatar", notNullValue());
    }
    @Test
     void testGetNonExistingUser() {
        ProjectConfig config = ConfigFactory.create(ProjectConfig.class);
        RestAssured.baseURI = config.baseUrl();
        int userId = 23; // Несуществующий id пользователя

        given()
                .when()
                .get("/api/users/" + userId)
                .then()
                .statusCode(404);

    }
}
