package tests;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

public class RegisterTest1 {

    @Test
    public void testRegisterUser_Positive() {
        RestAssured.baseURI = "https://reqres.in";

        given()
                .contentType(ContentType.JSON)
                .body("{ \"email\": \"eve.holt@reqres.in\", \"password\": \"pistol\" }")
                .when()
                .post("/api/register")
                .then()
                .statusCode(200)
                .body("id", notNullValue())
                .body("token", notNullValue());
    }

    @Test
    public void testRegisterUser_Negative() {
        RestAssured.baseURI = "https://reqres.in";

        given()
                .contentType(ContentType.JSON)
                .body("{ \"email\": \"sydney@fife\" }")
                .when()
                .post("/api/register")
                .then()
                .statusCode(400)
                .body("error", equalTo("Missing password"));
    }
    @Test
    public void test_users_positive(){
        RestAssured.baseURI = "https://reqres.in";
        given()
                .contentType(ContentType.JSON)
                .body("{\"name\": \"morpheus\", \"job\": \"zion resident\" }")
                .when()
                .put("api/users/2")
                .then()
                .statusCode(200)
                .body("name", equalTo("morpheus"))
                .body("job", equalTo("zion resident"))
                .body("updatedAt", notNullValue());
    }


    @Test
    public void testDeleteExistingUser() {
        RestAssured.baseURI = "https://reqres.in";
        int userId = 2; // Замените на существующий id пользователя

        given()
                .when()
                .delete("/api/users/" + userId)
                .then()
                .statusCode(204); // Ожидаемый код состояния 204 - успешное удаление
    }
    @Test
    public void testUpdateUser_Negative() {
        RestAssured.baseURI = "https://reqres.in";
        int userId = 23; // Несуществующий id пользователя

        given()
                .contentType(ContentType.JSON)
                .body("{ \"name\": \"21\", \"job\": \"000\" }")
                .when()
                .put("/api/users/" + userId)
                .then()
                .statusCode(404);

    }
    @Test
    public void testDeleteNonExistingUser() {
        RestAssured.baseURI = "https://reqres.in";
        int userId = 23; // несуществующий id пользователя

        given()
                .when()
                .delete("/api/users/" + userId)
                .then()
                .statusCode(404);

    }
    @Test
    public void testGetExistingUser() {
        RestAssured.baseURI = "https://reqres.in";
        int userId = 2; // Замените на существующий id пользователя

        given()
                .when()
                .get("/api/users/" + userId)
                .then()
                .statusCode(200)
                .body("data.id", equalTo(userId))
                .body("data.email", notNullValue())
                .body("data.first_name", notNullValue())
                .body("data.last_name", notNullValue())
                .body("data.avatar", notNullValue());
    }
    @Test
    public void testGetNonExistingUser() {
        RestAssured.baseURI = "https://reqres.in";
        int userId = 23; // Несуществующий id пользователя

        given()
                .when()
                .get("/api/users/" + userId)
                .then()
                .statusCode(404);

    }
}
