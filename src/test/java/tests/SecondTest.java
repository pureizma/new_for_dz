package tests;
import org.junit.Test;
import pages.HomePage;
import pages.HotelPopUp;
import pages.SearchPage;
public class SecondTest {
    HomePage homePage = new HomePage();

    SearchPage searchPage = new SearchPage();
    HotelPopUp hotelPopUp = new HotelPopUp();
    private String city = "Анталья";

    private String starsCount = "5";

    // Тест-кейс
    // Зайти на сайт www.booking.com
    // Ввести в поиске "Анталья"
    // Нажать на кнопку "Найти"
    // Нажать "Показать на карте"
    // Навести курсор на первый отель в карточке слева
    // Сохранить название отеля, количество звезд, среднюю оценку, количество отзывов, стоимость
    // Нажать прыгающий маркер
    // На открывшейся странице отеля проверить данные на соответствие

    @Test()
    public void successBookingFiveStarsTest() {
        homePage.openHomePage()
                .acceptCookies()
                .searchCity(city);

        searchPage.checkCityHeader(city)
                .closeAccountWindow()
                .clickShowOnMapButton();

        hotelPopUp.captureHotelData()
                  .clickOnMarker();

    }
}


