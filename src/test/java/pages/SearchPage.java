package pages;

import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.Condition;
import org.openqa.selenium.By;

import java.time.Duration;

import static com.codeborne.selenide.Condition.appear;
import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class SearchPage {
    private final By showOnMapButton = By.xpath("//button[@data-map-trigger-button='1']");
    private final By cityHeader = By.xpath("//h1");
    private final By mapLeftCards = By.xpath("//div[@data-testid='map-trigger']//button[contains(@class, 'a83ed08757')]//span[text()='Показать на карте']");
    private final By objectRate = By.xpath("//div[@id='filter_group_class_:r1e:']");

    private final String hotelCheckboxRate = "//input[@name='class=%s']";

    private final String rateInput = "//div[@id='filter_group_class_:r1o:']//input[@name='class=%s']";
    private final String rateInput2 = "//div[@data-filters-group='class' and @data-testid='filters-group']//div[@data-filters-item='class:class=%s']";

    private final By accountMenuCloseButton = By.xpath("//button[@aria-label='Скрыть меню входа в аккаунт.']");

    private final By ratingHotel = By.xpath("//div[@data-testid='rating-stars']");

    private final By star = By.cssSelector("span");

    public SearchPage closeAccountWindow() {
        $(accountMenuCloseButton).shouldBe(Condition.visible).click();
        return this;
    }

    public SearchPage checkCityHeader(String city) {
        $(cityHeader).shouldBe(Condition.visible, Duration.ofSeconds(10))
                .shouldHave(text(city));
        return this;
    }

    public SearchPage fillHotelCheckboxRate(String starsCount) {
//        $(objectRate).find(By.xpath(String.format(hotelCheckboxRate, starsCount))).click();
        $(By.xpath(String.format(rateInput2, starsCount)))
                .shouldBe(Condition.visible, Duration.ofSeconds(10))
                .scrollTo()
                .click();
        return this;
    }

    public SearchPage checkHotelRating(String rate) {
        $$(ratingHotel).forEach(x -> x.findAll(star).shouldHave(CollectionCondition.size(Integer.parseInt(rate))));
        return this;
    }
    public SearchPage clickShowOnMapButton() {
        // Находим элемент <button> с атрибутами data-map-trigger-button='1' и текстом "Показать на карте"
        $(mapLeftCards).shouldBe(Condition.visible, Duration.ofSeconds(10)) // Ожидаем появления элемента на странице
                .click(); // Кликаем по элементу
        return this;
    }
    public SearchPage waitForElementInsideMapContainer() {
        // Находим основной div с классом map_with_list__container
        $(By.className("map_with_list__container"))
                // Ожидаем появления элемента с классом b7d7ae581d внутри основного div
                .find(By.className("b7d7ae581d"))
                .shouldBe(appear);
        return this;
    }
}

