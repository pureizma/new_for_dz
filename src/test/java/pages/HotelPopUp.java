package pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;
import java.time.Duration;
import com.codeborne.selenide.Selenide;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import com.codeborne.selenide.WebDriverRunner;

import static com.codeborne.selenide.Condition.clickable;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;


public class HotelPopUp {
    private final By elementWithZIndex = By.xpath("//div[contains(@style, 'z-index: 101;') and contains(@style, 'position: absolute;')]");

    private final By firstHotelCard = By.cssSelector(".map_left_cards .b6ae4fce06");
    private final By hotelNameLocator = By.cssSelector("[data-testid='header-title']");
    private final By starCountLocator = By.cssSelector("[data-testid='rating-stars'] > span");
    private final By averageRatingLocator = By.cssSelector(".a3b8729ab1.d86cee9b25");
    private final By reviewCountLocator = By.cssSelector(".abf093bdfe.f45d8e4c32.d935416c47");
    private final By priceInfoLocator = By.cssSelector(".ac4a7896c7");

    public HotelPopUp captureHotelData() {
        $(firstHotelCard).shouldBe(Condition.visible, Duration.ofSeconds(20));


        // Получаем элемент отеля после ожидания
        SelenideElement hotelCard = $(firstHotelCard);


        // Название отеля
        String hotelName = hotelCard.findElement(hotelNameLocator).getText();

        // Количество звезд
        int starCount = hotelCard.findElements(starCountLocator).size();

        // Средняя оценка
        String averageRating = hotelCard.findElement(averageRatingLocator).getText();

        // Количество отзывов
        String reviewCount = hotelCard.findElement(reviewCountLocator).getText();

        // Цена
        String priceInfo = hotelCard.findElement(priceInfoLocator).getText();

        // Теперь вы можете использовать эти переменные для выполнения необходимых действий
        // Например, сохранить их в какие-то другие переменные или выполнить проверки

        // Пример вывода данных на консоль
        System.out.println("Hotel Name: " + hotelName);
        System.out.println("Star Count: " + starCount);
        System.out.println("Average Rating: " + averageRating);
        System.out.println("Review Count: " + reviewCount);
        System.out.println("Price Info: " + priceInfo);
        return this; // Возвращаем текущий объект HotelPopUp
    }

    public HotelPopUp clickOnMarker() {
        // Находим первый отель на странице
        WebElement firstHotel = Selenide.$(firstHotelCard);

        // Инициализируем объект Actions для выполнения действий с мышью
        Actions actions = new Actions(WebDriverRunner.getWebDriver());

        // Наводим мышь на первый отель
        $(firstHotelCard).hover();

        // Проверяем, поменялся ли z-index у какого-либо элемента на странице
        //Selenide.$(elementWithZIndex).shouldBe(Condition.visible, Duration.ofSeconds(10));
// Получение значения атрибута style элемента
        String styleAttributeValue = $(elementWithZIndex).getAttribute("style");
        // Извлечение позиции элемента из значения атрибута style
        String[] styleParts = styleAttributeValue.split(";");
        String left = "", top = "";
        for (String part : styleParts) {
            if (part.trim().startsWith("left")) {
                left = part.trim().split(":")[1].trim();
            } else if (part.trim().startsWith("top")) {
                top = part.trim().split(":")[1].trim();
            }
        }

// Вывод позиции элемента
        System.out.println("Left position: " + left);
        System.out.println("Top position: " + top);
        return this;
    }
}
